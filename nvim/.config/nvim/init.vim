set nocompatible

" Using vim-plug: https://github.com/junegunn/vim-plug
" Specify a directory for plugins
call plug#begin(stdpath('data') . '/plugged')

Plug 'vimwiki/vimwiki'
Plug 'lilydjwg/colorizer'
Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
Plug 'chriskempson/base16-vim'

" Initialize plugin system
call plug#end()

filetype plugin on
syntax on
set termguicolors " Force gui colors
let base16colorspace=256  " Access colors present in 256 colorspace


" Colorscheme
set background=dark
let g:gruvbox_contrast_dark = "hard"
let g:gruvbox_color_column = "bg0"
colorscheme base16-tomorrow-night

" Configure lightline
set laststatus=2
let g:lightline = {
      \ 'colorscheme': 'Tomorrow_Night',
      \ }

set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set smartcase
set noswapfile
set incsearch
set hidden

set colorcolumn=80

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

let mapleader = ' ' " Change leader key to space

" Window navigation
nnoremap <leader>c <C-w>c
nnoremap <leader>o <C-w>o
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l

" Configure vimwiki
let g:vimwiki_list = [{'path': '~/wiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_markdown_link_ext = 1
let g:vimwiki_folding = 'list'

command! Diary VimwikiDiaryIndex
augroup vimwikigroup
    autocmd!
    " automatically update links on read diary
    autocmd BufRead,BufNewFile diary.md VimwikiDiaryGenerateLinks
augroup end
