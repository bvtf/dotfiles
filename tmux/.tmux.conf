# Global Settings
# ---------------
setw -g default-terminal "xterm-256color"
setw -g mode-keys vi

set-option -g allow-rename off

# Set the base index for windows and pane to 1
set -g base-index 1
set -g pane-base-index 1

# Key bindings
# ------------

# Set window split
bind v split-window -h
bind b split-window

# Vim like pane navigation
bind -r h select-pane -L
bind -r j select-pane -D
bind -r k select-pane -U
bind -r l select-pane -R

# Vim like pane resize
bind -r C-h resize-pane -L 5
bind -r C-j resize-pane -D 5
bind -r C-k resize-pane -U 5
bind -r C-l resize-pane -R 5

# Window navigation
bind -n C-Right next-window
bind -n C-Left previous-window

# Moving Windows
bind -n C-S-Left swap-window -t -1
bind -n C-S-Right swap-window -t +1

# Reload config
bind r source-file ~/.tmux.conf \; display "Config Reloaded!"

# View processes
bind '~' split-window "exec htop"

# Open a man page in new window
bind / command-prompt -p "manpage:" "split-window 'exec man %%'"

# Simple theme
set -g status-position bottom
set -g status-bg '#282828'
#set -g status-bg colour234
set -g status-fg colour137
set -g status-left '#S> '
set -g status-right '#[fg=colour233,bg=colour241,bold] %d/%m #[fg=colour233,bg=colour245,bold] %H:%M:%S '
set -g status-right-length 50
set -g status-left-length 20

setw -g window-status-current-format '#I#[fg=colour250]:#[fg=colour255]#W#[fg=colour50]#F '
setw -g window-status-format '#I#[fg=colour237]:#[fg=colour250]#W#[fg=colour244]#F '
