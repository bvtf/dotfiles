# .bashrc

export EDITOR=nvim
export BROWSER=firefox

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# Minimalist prompt

export PS1="\[\e[0;37m\]\w \[\e[0;33m\]ϟ\[\033[00m\] "

# User specific aliases and functions
alias ls='ls --color=auto'
alias ll='ls -lF'
alias vim=nvim
alias vi=vim
alias v=vim
alias vrc='nvim ~/.config/nvim/init.vim'
